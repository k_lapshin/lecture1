package sbp;

import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;

public class User {

    private static int idCounter;
    private final int id ;
    private String name;
    private String Surname;
    private int age;
    private Date dateOfRegistration;
    private int phone;
    private long inn;
    private long snils;

    public User() {
        idCounter++;
        this.id=idCounter;
    }

    public User(String name, String surname, int age, int phone) {
        this.name = name;
        idCounter++;
        this.id=idCounter;
    }

    public User(String name, String surname, int age, int phone, long inn, long snils) {
        idCounter++;
        this.id=idCounter;
        this.name = name;
        Surname = surname;
        this.age = age;
        this.dateOfRegistration = new Date();
        this.phone = phone;
        this.inn = inn;
        this.snils = snils;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return Surname;
    }

    public int getAge() {
        return age;
    }

    public Date dateOfRegistration() {
        return dateOfRegistration;
    }

    private int getId(){
        return id;
    }

    public int getPhone() {
        return phone;
    }

    public long getInn() {
        return inn;
    }

    public long getSnils() {
        return snils;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", Name='" + name + '\'' +
                ", Surname='" + Surname + '\'' +
                ", Age=" + age +
                ", Date of registration=" + dateOfRegistration +
                ", Phone=" + phone +
                ", Inn=" + inn +
                ", Snils=" + snils +
                '}' + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return age == user.age && phone == user.phone && inn == user.inn && snils == user.snils && name.equals(user.name) && Surname.equals(user.Surname) && dateOfRegistration.equals(user.dateOfRegistration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, Surname, age, dateOfRegistration, phone, inn, snils);
    }
}
