package sbp.collections;

import java.util.Arrays;
import java.util.Collection;

/**
 * Класс для хранения массивов, с любым типом.
 *
 * @param <T>
 */
public class CustomArrayImpl<T> implements CustomArray<T> {
    /**
     * Константа емкости списка по умолчанию.
     */
    private int CAPACITY = 10;
    /**
     * Массив по умолчанию.
     */
    private final T[] EMPTY_ARRAY = (T[]) new Object[CAPACITY];
    /**
     * Переменная размера массива.
     */
    private int size;
    /**
     * Переменная с переходящим типом.
     */
    transient T[] arrayData;

    /**
     * Конструктор без параметров, создающий список по умолчанию.
     */
    public CustomArrayImpl() {
        this.arrayData = EMPTY_ARRAY;
    }

    /**
     * Конструктор с параметром емкость, создает массив с указаной емкостью.
     *
     * @param capacity емкость массива
     */
    public CustomArrayImpl(int capacity) {
        if (capacity > 0) {
            this.arrayData = (T[]) new Object[capacity];
        } else if (capacity == 0) {
            this.arrayData = EMPTY_ARRAY;
        } else {
            throw new IllegalArgumentException();

        }

    }

    /**
     * Конструктор принимающий параметр колекцию, создает массив из коллекции.
     *
     * @param c коллекция
     */
    public CustomArrayImpl(Collection<T> c) {
        Object[] a = c.toArray();
        if ((size = a.length) != 0) {
            arrayData = (T[]) a;
        }

    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size() == 0;
    }

    /**
     * Метод для увеличения емкости массива.
     *
     * @param minCapasity - фактический размер
     * @return возвращает увеличенный массив.
     */
    private Object[] increase(int minCapasity) {
        int oldCapacity = arrayData.length;
        if ((oldCapacity > 0 || EMPTY_ARRAY.length != minCapasity)) {
            int newCapacity = 2 * oldCapacity - oldCapacity/2;
            return arrayData = Arrays.copyOf(arrayData, newCapacity);
        } else {
            return arrayData = (T[]) new Object[minCapasity];
        }

    }

    /**
     * Метод для добавления обьекта.
     *
     * @param item      обьект.
     * @param arrayData массив.
     */
    private void addObject(Object item, Object[] arrayData) {
        if (getCapacity() == size()) {
            arrayData = increase(size);
        }
        arrayData[size] = item;
        size = size + 1;

    }

    /**
     * Метод добавления обькта.
     *
     * @param item обьект.
     * @return true, если добавление удачно.
     * @throws IllegalArgumentException, если обьект null.
     */

    public boolean add(T item) {

        if (item == null) {
            throw new IllegalArgumentException();
        }

        addObject(item, arrayData);
        return true;

    }

    /**
     * Метод для добавления массива,вставка в конец исходного массива.
     *
     * @param items массив для добавления.
     * @return true, если удачно.
     * @throws IllegalArgumentException, если обьект null.
     */
    @Override
    public boolean addAll(T[] items) {

        Object[] array = items;
        int length = items.length;
        if (length == 0) {
            throw new IllegalArgumentException();
        }
        if (array == null) {
            throw new IllegalArgumentException();
        }
        ensureCapacity(items.length);
        System.arraycopy(array, 0, arrayData, size, array.length);
        size = size + array.length;
        return true;

    }

    /**
     * Метод для добавления коллекции, вставка в конец исходного массива.
     *
     * @param items
     * @return
     * @throws IllegalArgumentException, если обьект null.
     */
    @Override
    public boolean addAll(Collection items) {
        Object[] array = items.toArray();
        if (array.length == 0) {
            throw new IllegalArgumentException();
        }
        ensureCapacity(array.length);
        System.arraycopy(array, 0, arrayData, size, array.length);
        size = size + array.length;
        return true;

    }

    /**
     * Метод для добавления массива, вставка с указаного индекса, со смещением исходного списка вправо.
     *
     * @param index - с какого индекса вставлять
     * @param items - массив для вставки.
     * @return true, если удачно.
     * @throws IllegalArgumentException, если обьект null.
     */
    @Override
    public boolean addAll(int index, T[] items) {
        Object[] array = items;
        if (array.length == 0) {
            throw new IllegalArgumentException();
        } else if (index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        ensureCapacity(array.length );
        if (this.size - index > 0) {
            System.arraycopy(arrayData, index, arrayData, index + array.length, this.size - index);
            System.arraycopy(array, 0, arrayData, index, array.length);
            this.size = this.size + array.length;
        }
        return true;
    }

    /**
     * Метод для получения элемента по индексу.
     *
     * @param index - индекс элемента.
     * @return элемент.
     * @throws ArrayIndexOutOfBoundsException , если индекс выходит за пределы допустимого.
     */
    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return arrayData[index];
    }

    /**
     * Метод для замены обьекта, по указаному индексу, на указаный обьект.
     *
     * @param index - индекс обьекта
     * @param item  обьект для замены
     * @return измененный обьект
     * @throws ArrayIndexOutOfBoundsException , если индекс выходит за пределы допустимого.
     */
    @Override
    public Object set(int index, Object item) {
        int length = arrayData.length;
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }

        arrayData[index] = (T) item;
        return item;


    }

    /**
     * Метод для удаления элемента по индексу.
     *
     * @param index - индекс для удаления.
     * @throws ArrayIndexOutOfBoundsException , если индекс выходит за пределы допустимого.
     */
    @Override
    public void remove(int index) {
        if (size < index || size < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }

        System.arraycopy(arrayData, index + 1, arrayData, index, size() - index);
        size = size - 1;
    }

    /**
     * Метод для удаления элемента по переданному обьекту.
     *
     * @param item - item
     * @return true, если успешно.      false, если не успешно
     */
    @Override
    public boolean remove(T item) {
        if (size == getCapacity()) {
            ensureCapacity(5);
        }
        for (int i = 0; i < size; i++) {
            T arrayDatum = arrayData[i];
            if (arrayDatum.equals(item)) {
                remove(i);
                return true;
            }

        }
        return false;
    }

    /**
     * Метод ищет переданый обьект.
     *
     * @param item - обьект.
     * @retur true, если есть совпадение.  false,если нет совпадения.
     */
    @Override
    public boolean contains(Object item) {
        for (int i = 0; i < size; i++) {
            T arrayDatum = arrayData[i];
            if (arrayDatum.equals(item)) {
                return true;
            }

        }
        return false;
    }

    /**
     * Метод возвращает индекс элемента, если он есть.
     *
     * @param item - обьект
     * @return индекс, если обьект присутствует. -1, если отсутствует.
     */
    @Override
    public int indexOf(Object item) {
        for (int i = 0; i < size; i++) {
            T arrayDatum = arrayData[i];
            if (arrayDatum.equals(item)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Метод для увеличения емкости масиива
     *
     * @param newElementsCount - новая емкость.
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        arrayData = Arrays.copyOf(arrayData, arrayData.length + newElementsCount);
    }

    /**
     * Метод возвращает емкость массива
     *
     * @return емкость.
     */
    @Override
    public int getCapacity() {
        return arrayData.length;
    }

    /**
     * Метод для разворота списка
     */
    @Override
    public void reverse() {
        T[] array = (T[]) new Object[size];
        for (int i = 0; i < size; i++) {
            array[(size - 1) - i] = arrayData[i];
        }
        arrayData = array;

    }

    /**
     * Метод для копии массива
     *
     * @return
     */
    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = arrayData[i];
        }
        return array;
    }

    @Override
    public String toString() {
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = arrayData[i];
        }
        return
                Arrays.toString(array)
                ;
    }
}
