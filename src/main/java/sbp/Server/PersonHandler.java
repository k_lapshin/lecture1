package sbp.Server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.JSONObject;
import sbp.comparator.Person;
import sbp.comparator.PersonDAO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class PersonHandler implements HttpHandler {

    private PersonDAO personDAO;

    public PersonHandler(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    /**
     * Метод для принятия запроса от клиента и вызова сопутствующих методов
     *
     * @param exchange запрос от клиента
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        OutputStream outputStream = exchange.getResponseBody();

        if (exchange.getRequestMethod().equalsIgnoreCase("GET")) {
            handleGet(exchange);
        } else if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
            handlePost(exchange);
        } else if (exchange.getRequestMethod().equalsIgnoreCase("DELETE")) {
            handleDelete(exchange);
        } else if (exchange.getRequestMethod().equalsIgnoreCase("PUT")) {
            handleUpdate(exchange);

        }

    }

    /**
     * Обработчик Get запросов
     *
     * @param exchange запрос от клиента
     * @throws IOException
     */
    private void handleGet(HttpExchange exchange) throws IOException {
        OutputStream outputStream = exchange.getResponseBody();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html>")
                .append("</body>")
                .append("</h1>")
                .append("All Person: " + getAllPerson())
                .append("</h1>")
                .append("</body>")
                .append("</html>");

        String htmlStr = stringBuilder.toString();

        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Обработчик Post запросов
     * @param exchange запрос от клиента
     * @throws IOException
     */
    private void handlePost(HttpExchange exchange) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        String jsonStr = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                .lines()
                .collect(Collectors.joining());

        JSONObject jsonObject = new JSONObject(jsonStr);

        int UI = jsonObject.getInt("uniqueNumber");
        String name = jsonObject.toMap().get("name").toString();
        String city = jsonObject.toMap().get("city").toString();
        int age = jsonObject.getInt("age");

        Person person = new Person(UI, name, city, age);
        stringBuilder.append("<html>")
                .append("</body>")
                .append("</h1>")
                .append("Create Person: " + createPerson(person))
                .append("</h1>")
                .append("</body>")
                .append("</html>");

        String htmlStr = stringBuilder.toString();
        OutputStream outputStream = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Обработчик Delete запросов
     * @param exchange запрос от клиента
     * @throws IOException
     */
    private void handleDelete(HttpExchange exchange) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String jsonStr = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                .lines()
                .collect(Collectors.joining());

        JSONObject jsonObject = new JSONObject(jsonStr);

        int UI = jsonObject.getInt("uniqueNumber");

        stringBuilder.append("<html>")
                .append("</body>")
                .append("</h1>")
                .append("Delete Person: " + deletePerson(UI))
                .append("</h1>")
                .append("</body>")
                .append("</html>");

        String htmlStr = stringBuilder.toString();
        OutputStream outputStream = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, htmlStr.length());

        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }

    /**
     * Обработчик Update запросов
     * @param exchange запрос от клиента
     * @throws IOException
     */
    private void handleUpdate(HttpExchange exchange) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String jsonStr = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                .lines()
                .collect(Collectors.joining());

        JSONObject jsonObject = new JSONObject(jsonStr);

        int UI = jsonObject.getInt("uniqueNumber");
        String name = jsonObject.toMap().get("name").toString();

        stringBuilder.append("<html>")
                .append("</body>")
                .append("</h1>")
                .append("Create Person: " + updatePerson(UI, name))
                .append("</h1>")
                .append("</body>")
                .append("</html>");

        String htmlStr = stringBuilder.toString();

        exchange.sendResponseHeaders(200, htmlStr.length());
        OutputStream outputStream = exchange.getResponseBody();
        outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        outputStream.close();
    }


    /**
     * Метод для изменения Person
     *
     * @param ui   уникальный идентификатор
     * @param name Имя Person
     * @return сообщение об удачном обновлении
     */
    private String updatePerson(int ui, String name) {
        try {
            this.personDAO.updatePerson(ui, name);
            return "Update success";
        } catch (Exception e) {
            return "error" + e.getMessage();
        }

    }

    /**
     * Метод для удаления Person
     *
     * @param ui уникальный идентификатор
     * @return сообщение об удачном обновлении
     */
    private String deletePerson(int ui) {
        try {
            this.personDAO.deletePerson(ui);
            return "Delete success";
        } catch (Exception e) {
            return "error" + e.getMessage();
        }
    }

    /**
     * Метод для создания Person
     *
     * @param person @see Person
     * @return сообщение об удачном обновлении
     */
    private String createPerson(Person person) {
        try {
            this.personDAO.addPerson(person);
            return "Add success";
        } catch (Exception e) {
            return "error" + e.getMessage();
        }
    }

    /**
     * Метод для отображения таблицы Person
     *
     * @return Таблицу Person
     */
    private String getAllPerson() {

        return this.personDAO.readTable().toString();
    }

}
