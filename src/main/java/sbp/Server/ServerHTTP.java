package sbp.Server;


import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.spi.HttpServerProvider;
import sbp.comparator.PersonDAO;
import sbp.comparator.ServiceDAO;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class ServerHTTP {

    /**
     * Имя хоста
     */
    private static final String HOSTNAME = "localhost";

    /**
     * Порт
     */
    private static final int PORT = 8080;

    /**
     * Запуск сервера
     * @throws IOException
     */
    public static void serverStart() throws IOException {
        HttpServerProvider serverProvider = HttpServerProvider.provider();

        HttpServer server = serverProvider.createHttpServer(new InetSocketAddress(HOSTNAME, PORT), 0);

        server.createContext("/welcome", new WelcomeHandler());
        server.createContext("/persons", new PersonHandler(new PersonDAO(new ServiceDAO())));
        server.setExecutor(Executors.newFixedThreadPool(4));
        server.start();
        System.out.println("Server start ... ");
    }

}

