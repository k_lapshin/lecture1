package sbp;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;
import sbp.comparator.Person;
import sbp.comparator.PersonDAO;
import sbp.restTemplate.RestServer;
import sbp.springConfig.SpringConfig;

@SpringBootApplication
public class Sbp {


    public static void main(String[] args) throws Exception {

        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        PersonDAO personDAO = context.getBean(PersonDAO.class);

        SpringApplication.run(Sbp.class, args);

        Person person = new Person(4, "Television", "Samsung", 1145);
        Person person2 = new Person(4, "Telefon", "Samsung", 1145);
        personDAO.createTable();
        RestServer restServer = new RestServer();
        restServer.getHello();
        restServer.postHello(person2);
        restServer.putHello(person);
        restServer.deleteHello(person);
    }
}


