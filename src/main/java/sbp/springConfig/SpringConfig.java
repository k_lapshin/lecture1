package sbp.springConfig;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import sbp.comparator.PersonDAO;
import sbp.comparator.ServiceDAO;

import javax.inject.Qualifier;
import java.time.Duration;

@Configuration
@PropertySource("app.properties")
@ComponentScan("sbp.comparator")
@ComponentScan("sbp.restTemplate")
public class SpringConfig {



}
