package sbp.restTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import sbp.comparator.Person;
import sbp.comparator.PersonDAO;

@RestController
@RequestMapping("hello")
public class Controller {
    /**
     * Экземпляр класса для подключения к бд
     */
    @Autowired
    PersonDAO personDAO;

    /**
     * Конструктор
     * @param personDAO
     */
    public Controller(PersonDAO personDAO){
        this.personDAO = personDAO;
    }

    /**
     * Обработка get запроса
     * @return список person в формате строки
     */
    @GetMapping("/get")
    public ResponseEntity<String> sampleGetMethod() {
        System.out.println("hello");
        String result = personDAO.readTable().toString();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Обработка post запроса
     * @param person принимает person и записывает в бд
     * @return возвращает имя person
     */
    @PostMapping("/post")
    public ResponseEntity<String> samplePostMethod(@RequestBody Person person) {
       personDAO.addPerson(person);
        return new ResponseEntity<>(person.getName(), HttpStatus.OK);
    }

    /**
     * Обработка put запроса
     * @param person принимает person и перезаписывает в бд
     * @return возвращает имя person
     */
    @PutMapping("/put")
    public ResponseEntity<String> samplePutMethod(@RequestBody Person person){
        personDAO.updatePerson(person.getUniqueNumber(), person.getName());
        return new ResponseEntity<>(person.getName(), HttpStatus.OK);
    }

    /**
     * Обработка delete запроса
     * @param person принимает person и удаляет из бд
     * @return возвращает имя person
     */
    @DeleteMapping("/delete")
    public ResponseEntity<String> sampleDeleteMethod(@RequestBody Person person){
        personDAO.deletePerson(person.getUniqueNumber());
        return new ResponseEntity<>(person.getName(), HttpStatus.OK);
    }


    }
