package sbp.restTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import sbp.comparator.Person;

@RestController
@RequestMapping("/resthello")
public class RestServer {

    /**
     * Экземпляр класса RestTemplate
     */
    private RestTemplate restTemplate;

    /**
     * Конструктор
     */
    @Autowired
    public RestServer(){
        restTemplate = new RestTemplateBuilder().build();
    }

    /**
     * Конструктор
     */
    public RestServer(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    /**
     * Клиент для оправки запроса get на адрес "http://localhost:8080/hello/get"
     * @return возвращает список всех Person в бд
     */
    @GetMapping("/get")
    public String getHello() {

        String url
                = "http://localhost:8080/hello/get";
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        String body = responseEntity.getBody();
        return body;
    }

    /**
     * Клиент для оправки запроса post на адрес "http://localhost:8080/hello/post"
     * Добваляет новую person в бд
     * @param person
     * @return имя person
     */
    @PostMapping("/post")
    public String postHello(Person person) {
        String url
                = "http://localhost:8080/hello/post";
       ;

        ResponseEntity<Person> request = new ResponseEntity(person, HttpStatus.OK);
        String personCreateResponse = restTemplate.postForObject(url, request, String.class);
        return request.getBody().getName();
    }

    /**
     * Клиент для оправки запроса put на адрес "http://localhost:8080/hello/put"
     * Изменяет person в бд
     * @param person
     */
    @PutMapping("/put")
    public void putHello(Person person){
        String url
                = "http://localhost:8080/hello/put";

        HttpEntity<Person> request = new HttpEntity(person);
        restTemplate.exchange(url,HttpMethod.PUT, request,Void.class);

    }

    /**
     * Клиент для оправки запроса delete на адрес "http://localhost:8080/hello/delete"
     * Удаляет person в бд
     * @param person
     */
    @DeleteMapping("/delete")
    public void deleteHello(Person person){
        String url
                = "http://localhost:8080/hello/delete";

        HttpEntity<Person> request = new HttpEntity(person);
        restTemplate.exchange(url,HttpMethod.DELETE, request,Void.class);

    }
}
