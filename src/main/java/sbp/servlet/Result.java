package sbp.servlet;

import sbp.comparator.Person;
import sbp.comparator.PersonDAO;
import sbp.comparator.ServiceDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/result")
public class Result extends HttpServlet {
    public Result() {
    }

    /**
     * Метод для добавления Person в бд
     * @param req , request со страницы registration.jsp
     * @param resp, response на страницу result.jsp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            resp.setContentType("text/html;charset=UTF-8");
            req.setCharacterEncoding("UTF-8");

            String name = req.getParameter("name");
            Integer age = Integer.parseInt(req.getParameter("age"));
            String city = req.getParameter("city");
            Integer uniqueNumber = Integer.parseInt(req.getParameter("uniqueNumber"));

            Person person = new Person(uniqueNumber, name, city, age);
            PersonDAO personJDBC = new PersonDAO(new ServiceDAO());
            personJDBC.addPerson(person);

            req.setAttribute("clientName", person.getName());

            getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);

        } catch (IOException | NumberFormatException | ServletException e) {
            e.printStackTrace();
        }
    }
}
