package sbp.io;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.List;
import java.util.Scanner;

public class MyIOExampleFromNio2 {


    public MyIOExampleFromNio2() {
    }

    public MyIOExampleFromNio2(MyIOExampleFromNio2 myIOExample) {
    }

    /**
     * Создать объект класса {@link File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException {

        Path path = Paths.get(fileName);
        if (Files.exists(path)) {
            if (Files.isDirectory(path)) {
                System.out.println("Абсолютный путь " + path.toAbsolutePath());
                System.out.println("Родительский путь " + path.getParent());
                return true;
            }
            if (Files.isRegularFile(path)) {

                System.out.println("Время последнего изменения " + Files.getLastModifiedTime(path) +
                        " Размер файла в байтах " + Files.size(path));
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileInputStream} и {@link FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFilesByte(String sourceFileName, String destinationFileName) {

        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        try {

            byte[] buffer = Files.readAllBytes(sourcePath);
            Files.write(destinationPath, buffer);


            List<String> list = Files.readAllLines(Path.of(sourceFileName));
            List<String> list1 = Files.readAllLines(Path.of(destinationFileName));

            if (list.equals(list1)) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link BufferedInputStream} и {@link BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {

        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);

        BufferedReader bufferedReader =null;
        BufferedWriter bufferedWriter = null;

        try {
            if (Files.notExists(destinationPath)) {
                Files.createFile(destinationPath);
            }

            if (Files.exists(sourcePath)) {

                 bufferedReader = Files.newBufferedReader(sourcePath);
                 bufferedWriter = Files.newBufferedWriter(destinationPath);

                String lines;
                while ((lines = bufferedReader.readLine()) != null) {

                    bufferedWriter.write(lines + "\n");
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }

            } else {
                return false;
            }

            List<String> list = Files.readAllLines(Path.of(sourceFileName));
            List<String> list1 = Files.readAllLines(Path.of(destinationFileName));

            if (list.equals(list1)) {
                return true;
            }

        } catch (Exception e) {
            return false;
        } finally {
            bufferedReader.close();
            bufferedWriter.close();
        }
        return false;

    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link FileReader} и {@link FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFilesCopy(String sourceFileName, String destinationFileName) {

        Path sourcePath = Paths.get(sourceFileName);
        Path destinationPath = Paths.get(destinationFileName);
        try {
            if (Files.notExists(destinationPath)) {
                Files.createFile(destinationPath);
            }

            if (Files.exists(sourcePath)) {

                Files.copy(sourcePath, destinationPath, StandardCopyOption.REPLACE_EXISTING);

            } else {
                return false;
            }
            List<String> list = Files.readAllLines(Path.of(sourceFileName));
            List<String> list1 = Files.readAllLines(Path.of(destinationFileName));

            if (list.equals(list1)) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

}

