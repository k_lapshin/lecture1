package sbp.io;


import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.Scanner;

public class MyIOExample {


    public MyIOExample() {
    }

    public MyIOExample(MyIOExample myIOExample) {
    }

    /**
     * Создать объект класса {@link File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     * - абсолютный путь
     * - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     * - размер
     * - время последнего изменения
     * Необходимо использовать класс {@link File}
     *
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String fileName) throws IOException {

        File file = new File(fileName);

        if (file.exists()) {
            System.out.println("Абсолютный путь " + file.getAbsolutePath());
            System.out.println("Родительский путь " + file.getParent());

            if (file.isFile()) {
                Instant instant = Instant.ofEpochMilli(file.lastModified());
                System.out.println("Время последнего изменения " + instant +
                        " Размер файла в байтах " + file.length());
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName) throws IOException {



        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            FileWriter writer = new FileWriter(destinationFileName);
            writer.write("");
            writer.close();

            inputStream = new FileInputStream(sourceFileName);
            outputStream = new FileOutputStream(destinationFileName);
            byte[] buffer = new byte[sourceFileName.length()];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

        } catch (Exception e) {
            return false;
        } finally {
            inputStream.close();
            outputStream.close();

        }

        List<String> list = Files.readAllLines(Path.of(sourceFileName));
        List<String> list1 = Files.readAllLines(Path.of(destinationFileName));

        if (list.equals(list1)) {
            return true;
        }

        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {

        int file = sourceFileName.length();

        InputStream inputStream = null;
        OutputStream outputStream = null;

        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        try {
            FileWriter writer = new FileWriter(destinationFileName);
            writer.write("");
            writer.close();

            inputStream = new FileInputStream(sourceFileName);
            outputStream = new FileOutputStream(destinationFileName);

            bufferedInputStream = new BufferedInputStream(inputStream, 1);
            bufferedOutputStream = new BufferedOutputStream(outputStream, 1);

            bufferedInputStream.transferTo(bufferedOutputStream);


        } catch (Exception e) {
            return false;
        } finally {
            bufferedInputStream.close();
            bufferedOutputStream.close();

        }
        List<String> list = Files.readAllLines(Path.of(sourceFileName));
        List<String> list1 = Files.readAllLines(Path.of(destinationFileName));


        if (list.equals(list1)) {
            return true;
        }

        return false;

    }


    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     *
     * @param sourceFileName      - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) throws Exception {

        try {
            FileWriter writer = new FileWriter(destinationFileName);
            writer.write("");
            writer.close();

            FileReader fileReader = new FileReader(sourceFileName);
            Scanner scanner = new Scanner(fileReader);

            FileWriter fileWriter = new FileWriter(destinationFileName);

            int length;

            while ((length = fileReader.read()) != -1) {

                fileWriter.write(length);
                fileWriter.flush();
            }
        } catch (Exception e) {
            return false;
        } finally {

        }
        List<String> list = Files.readAllLines(Path.of(sourceFileName));
        List<String> list1 = Files.readAllLines(Path.of(destinationFileName));


        if (list.equals(list1)) {
            return true;
        }

        return false;
    }

}

