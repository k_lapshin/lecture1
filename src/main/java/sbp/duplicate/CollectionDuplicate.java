package sbp.duplicate;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionDuplicate {

    /**
     * Метод принимает коллекцию и возвращает коллекцию дубликатов,
     * если дубликаты отсутствуют возвращает пустую коллекцию
     *
     * @param collection коллекция на вход
     * @param <T> любого типа
     * @return коллекцию из дубликатов
     */
    public <T> Collection<T> collectionDuplicate(Collection<T> collection) {
        List<T> list = new ArrayList<>();

        Object[] array = collection.toArray();
        Object object = array[0];
        for (int i = 0; i < collection.size(); i++) {
            for (int j = i + 1; j < collection.size(); j++) {
                if (array[i].equals(array[j])) {
                    object = array[i];
                    list.add((T) object);
                }
            }
        }
        return (Collection<T>) list;
    }

    /**
     * Метод принимает коллекцию и возвращает коллекцию дубликатов,
     * если дубликаты отсутствуют возвращает пустую коллекцию
     *
     * @param collection коллекция на вход
     * @param <T> любого типа
     * @return коллекцию из дубликатов
     */
    public <T> Collection<T> collectionDuplicate2(Collection<T> collection) {
        Set<T> list = new HashSet<>();
        List<T> collection1 = (List<T>) collection;
        Consumer action = (value) -> {
            int count = 0;
            for (int i = 0; i < collection1.size(); i++) {
                Object value2 = collection1.get(i);
                if (value2.equals(value)) {
                    count++;
                    if (count > 1) {
                        list.add((T) value);
                    }
                }
            }
        };

        Stream stream = collection.stream();
        stream.forEach(action);
        return (Collection<T>) list;
    }

    /**
     * Метод принимает коллекцию и возвращает коллекцию дубликатов,
     * если дубликаты отсутствуют возвращает пустую коллекцию
     *
     * @param collection коллекция на вход
     * @param <T> любого типа
     * @return коллекцию из дубликатов
     */
    public <T> Collection<T> collectionDuplicate3(Collection<T> collection) {
        List<T> list = new ArrayList<>();

        Set<T> listDuplicate = new HashSet();

        Set<T> stream = collection.stream()
                .filter(e -> !listDuplicate.add(e))
                .collect(Collectors.toSet());


        return (Collection<T>) stream;
    }

    /**
     * Принимает строку состоящую из скобочек, проверяет на правильность закрытия
     *
     * @param s строка
     * @return true , если правильно закрыты. false, елси не правильно.
     */
    public boolean checkBrackets(String s) {
        Stack<Character> stack = new Stack();
        char[] array = s.toCharArray();

        int count = 0;
        for (int i = 0; i < array.length; i++) {

            if ((array[i] == '(') || (array[i] == '[') || (array[i] == '{')) {
                stack.push(array[i]);
                count++;

            }
            if ((array[i] == ')') || (array[i] == ']') || (array[i] == '}')) {
                if (!stack.isEmpty())
                {
                    if ((array[i]) == (')') && (char) stack.pop() == ('(')) {
                        count--;
                    }
                    if ((array[i]) == (']') && (char) stack.pop() == ('[')) {
                        count--;
                    }
                    if ((array[i]) == ('}') && (char) stack.pop() == ('{')) {
                        count--;
                    }
                } else
                {
                    count++;
                }

            }

        }
        return count == 0;
    }

}
