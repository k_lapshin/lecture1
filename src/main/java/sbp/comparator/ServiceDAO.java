package sbp.comparator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Component
public class ServiceDAO  {

    private String url;

    /**
     * Путь к базе данных
     */
    @Value(value = "${url}")
    public void setUrl(String url) {
        this.url = url;
    }


    /**
     * Блокировщик чтения/записи для синхронизации потоков
     */
    ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    /**
     * Пул потоков с кэшированием, потоки убираются через минуту.
     */
    ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * Метод для выполнения SELECT запросов
     *
     * @param query запрос
     * @return Возвращает List<Person>.
     * @throws SQLException, InterruptedException
     */

    public synchronized List<Person> executeQuery(String query) throws SQLException {
        List<Person> personList = new ArrayList<>();

        List<Callable<List<Person>>> personCallList = new ArrayList<>();
        Callable task = new Callable() {
            @Override
            public List<Person> call() throws Exception {
                Class.forName("org.sqlite.JDBC");
                Statement statement = null;
                ResultSet result = null;
                try (Connection connection = DriverManager.getConnection(url)) {
                    statement = connection.createStatement();
                    result = statement.executeQuery(query);

                    while (result.next()) {

                        int uniqueNumber = Integer.parseInt((result.getString("uniqueNumber")));
                        String name = result.getString("name");
                        String city = result.getString("city");
                        int age = Integer.parseInt(result.getString("age"));

                        Person person = new Person(uniqueNumber, name, city, age);
                        personList.add(person);
                    }
                    return personList;
                } finally {
                    System.out.println(personList);
                    statement.close();
                    result.close();
                }

            }

        };
        personCallList.add(task);
        try {
            List<Future<List<Person>>> future = executorService.invokeAll(personCallList);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return personList;
    }

    /**
     * Метод для выполнения INSERT, UPDATE, DELETE, CREATE TABLE, DROP TABLE запросов
     *
     * @param update запрос
     * @return Возвращает true, если успешно. false при неудаче.
     * @throws SQLException, InterruptedException
     */

    public synchronized boolean executeUpdate(String update) throws SQLException {

        List<Callable<Boolean>> personCall = new ArrayList<>();

        Callable task = new Callable() {
            @Override
            public Object call() throws Exception {
                Class.forName("org.sqlite.JDBC");
                readWriteLock.writeLock().lock();
                try (Connection connection = DriverManager.getConnection(url)) {
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(update);
                    statement.close();
                    return true;
                } catch (SQLException e) {
                    System.out.println("executeUpdate: " + e.getMessage());
                    return false;
                } finally {

                    readWriteLock.writeLock().unlock();
                }

            }
        };
        personCall.add(task);
        try {
            List<Future<Boolean>> futures = executorService.invokeAll(personCall);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

}


