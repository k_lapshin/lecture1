package sbp.comparator;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator<Integer> {

    /**
     * Метод для определения порядка сравнения обьектов
     * Сначала четные, потом нечетный числа. Упорядоченный по возрастанию
     * @param o1
     * @param o2
     * @return возвращает 0, если обьекты равны.
     * @return -1, если вызывающий обьект, меньше обьекта переданного в качестве параметра.
     * @return 1, если вызывающий обьект, больше переданного в качестве параметра.
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        if (o1 == null || o2 == null) {
            throw new NullPointerException();
        }
        if (o1 == o2) {
            return 0;
        }

        if ((o1 % 2 == 0) && (o2 % 2 != 0)) {
            return -1;
        } else if ((o1 % 2 != 0) && (o2 % 2 == 0)) {
            return 1;
        } else if ((o1 % 2 == 0) && (o2 % 2 == 0)) {
            if (o1 > o2) {
                return 1;
            } else {
                return -1;
            }
        } else if ((o1 % 2 != 0) && (o2 % 2 != 0)) {
            return 0;
        }
        return 0;
    }
}