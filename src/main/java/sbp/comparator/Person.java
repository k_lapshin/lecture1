package sbp.comparator;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Objects;


public class Person implements Comparable<Person>, Serializable {
    /**
     * Поле uniqueNumber
     */
    private int uniqueNumber;
    /**
     * Поле Имя
     */
    private String name;
    /**
     * Поле Город
     */
    private String city;
    /**
     * Поле Возраст
     */
    private int age;

    public Person(){

    }

    /**
     * Конструктор - создание нового объекта
     *
     * @throws NullPointerException
     * @see Person#Person(int, String, String, int)
     */
    public Person(int uniqueNumber, String name, String city, int age) {
        if (name == null || city == null || age == 0) {
            throw new NullPointerException();
        }
        this.uniqueNumber = uniqueNumber;
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public int getUniqueNumber() {
        return uniqueNumber;
    }

    public String getName() {
        return name;
    }


    public String getCity() {
        return city;
    }


    public int getAge() {
        return age;
    }

    /**
     * Метод сравнения обьектов
     *
     * @param o принимаемый параметр
     * @return true, если ссылки на обьекты равны или обьекты равны
     * @return false, если принимаемы обьект null или классы обьектов различны
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name) && Objects.equals(city, person.city);
    }

    /**
     * Генерирует hashCode
     *
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, city, age);
    }

    /**
     * Метод для определения порядка сравнения обьектов
     * Сравнение по полям
     *
     * @param o - обьект переданный в качестве парамтра.
     * @return возвращает 0, если обьекты равны.
     * @return -1, если вызывающий обьект, меньше обьекта переданного в качестве параметра.
     * @return 1, если вызывающий обьект, больше переданного в качестве параметра.
     */
    @Override
    public int compareTo(Person o) {
        int result = this.city.compareToIgnoreCase(o.city);
        if (result == 0) {
            return this.name.compareToIgnoreCase(o.name);
        } else
            return result;
    }

    @Override
    public String toString() {
        return "Person{" +
               " uniqueNumber: " + uniqueNumber + '\''+
                " name:'" + name + '\'' +
                ", city:'" + city + '\'' +
                ", age:" + age +
                '}';
    }


}
