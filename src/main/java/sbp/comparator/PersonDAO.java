package sbp.comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class PersonDAO {


    /**
     * Ссылка на экземпляр класса для подключения к бд
     *
     * @see ServiceDAO
     */

    private ServiceDAO serviceDAO;

    /**
     * Конструктор
     *
     * @param serviceDAO
     */
    @Autowired
    public PersonDAO( ServiceDAO serviceDAO) {
        System.out.println("PersonDAOIN");
        this.serviceDAO = serviceDAO;
    }


    /**
     * Метод для создания таблицы.
     *
     * @throws SQLException
     * @see ServiceDAO#executeUpdate(String)
     */
    public boolean createTable() {


        String sql = "create table  IF NOT EXISTS person  (" +
                " Id INTEGER NOT NULL PRIMARY KEY ," +
                " UniqueNumber unique,  " +
                "Name VARCHAR(32)," +
                "City VARCHAR(32) , " +
                "Age integer" + ");";
        boolean result = false;
        try {
            result = serviceDAO.executeUpdate(sql);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        }


    }

    /**
     * Метод для удаления таблицы.
     *
     * @throws SQLException
     * @see ServiceDAO#executeUpdate(String)
     */
    public boolean dropTable() {


        String sql = "drop table person ";
        boolean result = false;
        try {
            result = serviceDAO.executeUpdate(sql);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        }


    }

    /**
     * Метод для удаления строки по uniqueNumber из таблицы.
     *
     * @throws SQLException
     * @see ServiceDAO#executeUpdate(String)
     */
    public boolean deletePerson(int uniqueNumber) {

        String sql = "delete from person where uniqueNumber ='" + uniqueNumber + "';";

        boolean result = false;
        try {
            result = serviceDAO.executeUpdate(sql);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        }


    }

    /**
     * Метод для добавления строки в таблицу.
     *
     * @param person
     * @throws SQLException
     * @see ServiceDAO#executeUpdate(String)
     * @see Person#Person(int, String, String, int)
     */
    public boolean addPerson(Person person) {


        String sql = String.format("insert or IGNORE into person" +
                        "('uniqueNumber', 'name' , 'city' , 'age' )" +
                        "VALUES ('%s','%s','%s','%s' )",
                person.getUniqueNumber(), person.getName(), person.getCity(), person.getAge());
        boolean result = false;
        try {
            result = serviceDAO.executeUpdate(sql);
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        }


    }

    /**
     * Метод для изменения поля name, по uniqueNumber.
     *
     * @param uniqueNumber
     * @param name
     * @throws SQLException
     * @see ServiceDAO#executeUpdate(String)
     */
    public boolean updatePerson(int uniqueNumber, String name) {


        String sql = "update person set name = '" + name + "' where uniqueNumber ='" + uniqueNumber + "';";
        boolean result = false;
        try {
            result = serviceDAO.executeUpdate(sql);

            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        }


    }

    /**
     * Метод для вывода всех строк таблицы.
     *
     * @return List
     * @throws SQLException
     * @see ServiceDAO#executeQuery(String)
     */
    public List<Person> readTable() {

        String sql = "select * from person";
        List<Person> personList = new ArrayList<>();

        try {
            personList = serviceDAO.executeQuery(sql);
            return personList;
        } catch (SQLException e) {
            e.printStackTrace();
            return personList;
        }

    }


    /**
     * Метод для вывода строки, поuniqueNumber.
     *
     * @param uniqueNumber
     * @return null, если в таблице не существует переданного uniqueNumber.
     * @return Person
     * @throws SQLException
     * @see ServiceDAO#executeQuery(String)
     */
    public Person readTableUniqueNumber(int uniqueNumber) {

        String sql = "select * from person where uniqueNumber='" + uniqueNumber + "';";
        List<Person> personList = null;
        Person person = null;
        try {
            personList = serviceDAO.executeQuery(sql);
            person = personList.get(0);
            return person;
        } catch (SQLException e) {
            e.printStackTrace();
            return person;
        }


    }

}
