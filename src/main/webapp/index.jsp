<%@ page import="sbp.comparator.PersonDAO" %>
<%@ page import="sbp.comparator.ServiceDAO" %>
<%@ page import="java.util.List" %>
<%@ page import="sbp.comparator.Person" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <style type="text/css">
        <%@include file="resources/css/style.css" %>
    </style>
</head>
<body>
<h2>Какое либо мероприятие</h2>
<p></p>
<div> Заполнение тесктом</div>
<h4>Список гостей</h4>

<%
    PersonDAO personJDBC = new PersonDAO(new ServiceDAO());

    List<Person> personList = personJDBC.readTable();
    out.print("<ol>");
    for (Person person : personList) {
        out.print(" <li> Имя: " + person.getName() +
                " Город: " + person.getCity() +
                " Возраст: " + person.getAge() + "</li>");
    }
    out.print("</ol>");
%>

<p><a href="/index/registration.jsp">Регистрация</a></p>
<p><a href="/index/author.jsp">Автор</a></p>
</body>
</html>