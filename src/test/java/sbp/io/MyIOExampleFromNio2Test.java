package sbp.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyIOExampleFromNio2Test extends MyIOExampleFromNio2 {

    /**
     * Проверка метода
     * @see MyIOExampleFromNio2#workWithFile(String)  на копирование файлов
     */

    @Test
    void testWorkWithFile() {
        Path path = Paths.get("MyFileTest.txt");
        if (Files.notExists(path)) {

            try {
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        MyIOExampleFromNio2 myIOExample = Mockito.mock(MyIOExampleFromNio2.class);

        try {
            Mockito.when(myIOExample.workWithFile(String.valueOf(path.getFileName()))).thenReturn(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(path.toAbsolutePath());

        Assertions.assertEquals(true, Files.isRegularFile(path));
    }


    /**
     * Проверка метода
     * @see MyIOExampleFromNio2#copyFilesByte(String, String)   на копирование файлов
     */

    @Test
    void testCopyFileByte() throws IOException {
        Path origin = Paths.get("MyTest.txt");
        Path file = Paths.get("MyFileTest.txt");
        Path fileCopy = Paths.get("MyFileTestCopy.txt");


        MyIOExampleFromNio2 myIOExample = Mockito.mock(MyIOExampleFromNio2.class);

        Mockito.when(myIOExample.copyFilesByte(file.toFile().getName(), file.toFile().getName())).thenReturn(true);

        MyIOExampleFromNio2 myIOExampleTest = new MyIOExampleFromNio2(myIOExample);

        Mockito.when(myIOExample.copyFilesByte(file.toFile().getName(), file.toFile().getName())).thenReturn(true);


        myIOExampleTest.copyFilesByte(file.toFile().getName(), file.toFile().getName());

        Files.copy(Path.of(file.toFile().getName()), new FileOutputStream(file.toFile().getName()));

        List<String> oList = Files.readAllLines(Path.of(file.toFile().getName()));
        List<String> list = Files.readAllLines(Path.of(file.toFile().getName()));
        List<String> list1 = Files.readAllLines(Path.of(file.toFile().getName()));

        Assertions.assertEquals(oList, list);
        Assertions.assertEquals(list1, list);
    }


    /**
     * Проверка метода
     * @see MyIOExampleFromNio2#copyBufferedFile(String, String)  на копирование файлов
     */

    @Test
    void testCopyBufferedFile() throws IOException {

        Path origin = Paths.get("MyTest.txt");
        Path file = Paths.get("MyFileTest.txt");
        Path fileCopy = Paths.get("MyFileTestCopy.txt");


        MyIOExampleFromNio2 myIOExample = Mockito.mock(MyIOExampleFromNio2.class);

        Mockito.when(myIOExample.copyBufferedFile(file.toFile().getName(), file.toFile().getName())).thenReturn(true);

        MyIOExampleFromNio2 myIOExampleTest = new MyIOExampleFromNio2(myIOExample);


        Mockito.when(myIOExample.copyBufferedFile(file.toFile().getName(), file.toFile().getName())).thenReturn(true);


        myIOExampleTest.copyBufferedFile(file.toFile().getName(), file.toFile().getName());

        Files.copy(Path.of(file.toFile().getName()), new FileOutputStream(file.toFile().getName()));

        List<String> oList = Files.readAllLines(Path.of(file.toFile().getName()));
        List<String> list = Files.readAllLines(Path.of(file.toFile().getName()));
        List<String> list1 = Files.readAllLines(Path.of(file.toFile().getName()));

        Assertions.assertEquals(oList, list);
        Assertions.assertEquals(list1, list);
    }


    /**
     * Проверка метода
     * @see MyIOExampleFromNio2#copyFilesCopy(String, String)  на копирование файлов
     */

    @Test
    void testCopyFilesCopy() {
        Path origin = Paths.get("MyTest.txt");
        Path file = Paths.get("MyFileTest.txt");
        Path fileCopy = Paths.get("MyFileTestCopy.txt");

        try {
            MyIOExampleFromNio2 myIOExample = Mockito.mock(MyIOExampleFromNio2.class);

            Mockito.when(myIOExample.copyFilesCopy(file.toFile().getName(), file.toFile().getName())).thenReturn(true);

            MyIOExampleFromNio2 myIOExampleTest = new MyIOExampleFromNio2(myIOExample);

            myIOExampleTest.copyFilesCopy(file.toFile().getName(), file.toFile().getName());

            Files.copy(Path.of(file.toFile().getName()), new FileOutputStream(file.toFile().getName()));

            List<String> oList = Files.readAllLines(Path.of(file.toFile().getName()));
            List<String> list = Files.readAllLines(Path.of(file.toFile().getName()));
            List<String> list1 = Files.readAllLines(Path.of(file.toFile().getName()));

            Assertions.assertEquals(oList, list);
            Assertions.assertEquals(list1, list);

        } catch (Exception e) {

        }

        try {
            MyIOExampleFromNio2 myIOExample = Mockito.mock(MyIOExampleFromNio2.class);

            Mockito.when(myIOExample.copyFilesCopy(file.toFile().getName(), file.toFile().getName())).thenReturn(true);

            MyIOExampleFromNio2 myIOExampleTest = new MyIOExampleFromNio2(myIOExample);

            myIOExampleTest.copyFilesCopy(file.toFile().getName(), file.toFile().getName());

            Files.copy(Path.of(file.toFile().getName()), new FileOutputStream(file.toFile().getName()));

            List<String> oList = Files.readAllLines(Path.of(file.toFile().getName()));
            List<String> list = Files.readAllLines(Path.of(file.toFile().getName()));
            List<String> list1 = Files.readAllLines(Path.of(file.toFile().getName()));

            Assertions.assertEquals(oList, list);
            Assertions.assertEquals(list1, list);

        } catch (Exception e) {

        }

    }
}