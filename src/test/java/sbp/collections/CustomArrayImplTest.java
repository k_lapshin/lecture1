package sbp.collections;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.collections.CustomArrayImpl;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.Collection;


public class CustomArrayImplTest {

    CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
    ArrayList<Integer> integerArrayList;
    ArrayList<String> stringArrayList;

    public CustomArrayImplTest() {
    }

    /**
     * Проверка метода
     * @see CustomArrayImpl#size()
     */
    @Test
    void size() {

        customArray.add(1);
        customArray.add(1);
        Assertions.assertEquals(2, customArray.size());
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#isEmpty()
     */
    @Test
    void isEmpty() {

        boolean result = customArray.isEmpty();
        Assertions.assertEquals(true, result);
        customArray.add(1);
        result = customArray.isEmpty();
        Assertions.assertEquals(false, result);
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#add(Object)
     */
    @Test
    void addObject() {
        boolean result = customArray.add(1);
        Assertions.assertEquals(true, result);


        Assertions.assertThrows(RuntimeException.class, () -> customArray.add(null));

    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#addAll(Object[])
     */
    @Test
    void addAllObjectArray() {

        Integer[] array1 = new Integer[1];
        array1[0] = 1;
        boolean result = customArray.addAll(array1);
        Assertions.assertEquals(true, result);

        Integer[] array = {};
        Assertions.assertThrows(RuntimeException.class, () -> customArray.addAll(array));

    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#addAll(Collection)
     */
    @Test
    void testAddAllCollection() {
        integerArrayList = new ArrayList<>();

        Assertions.assertThrows(RuntimeException.class, () -> customArray.addAll(integerArrayList));

        integerArrayList = new ArrayList<>();
        integerArrayList.add(1);
        boolean result = customArray.addAll(integerArrayList);
        Assertions.assertEquals(true, result);
        Assertions.assertEquals(integerArrayList.size(), customArray.size());
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#addAll(int, Object[])
     */
    @Test
    void testAddAllIndexObject() {
        Integer[] array = {};
        Integer[] array1 = new Integer[2];
        array1[0] = 1;
        array1[1] = 1;
        customArray.add(1);
        customArray.add(2);

        boolean result;
        Assertions.assertThrows(RuntimeException.class, () -> customArray.addAll(1, array));

        Assertions.assertThrows(RuntimeException.class, () -> customArray.addAll(3, array1));

        result = customArray.addAll(1, array1);
        Assertions.assertEquals(true, result);
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#get(int)
     */
    @Test
    void get() {
        int i = 2;

        customArray.add(1);
        customArray.add(i);

        Assertions.assertEquals(i, customArray.get(1));
        Assertions.assertThrows(RuntimeException.class, () -> customArray.get(50));
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#set(int, Object)
     */
    @Test
    void set() {
        customArray.add(1);
        customArray.add(2);

        customArray.set(1, 3);

        Assertions.assertEquals(3, customArray.get(1));
        Assertions.assertThrows(RuntimeException.class, () -> customArray.set(5, 1));
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#remove(int)
     */
    @Test
    void removeIndex() {

        for(int i = 0;i<11;i++) {
            customArray.add(1 + i);
        }
        Assertions.assertThrows(RuntimeException.class, () -> customArray.remove(-1));

        customArray.remove(11);
        int result = customArray.size();
        Assertions.assertEquals(10, result);


        Assertions.assertThrows(RuntimeException.class, () -> customArray.remove(11));
;


    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#remove(Object)
     */
    @Test
    void removeObject() {
        customArray.add(1);
        customArray.add(2);
        Integer i = 1;

        boolean result1 = customArray.remove(i);
        int result = customArray.size();

        Assertions.assertEquals(1, result);
        Assertions.assertEquals(true, result1);
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#contains(Object)
     */
    @Test
    void contains() {
        customArray.add(1);
        customArray.add(2);
        Integer i = 1;

        boolean result = customArray.contains(i);
        boolean result1 = customArray.contains(4);

        Assertions.assertEquals(true, result);
        Assertions.assertEquals(false, result1);
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#indexOf(Object)
     */
    @Test
    void indexOf() {
        customArray.add(1);
        customArray.add(2);

        int result = customArray.indexOf(1);
        int result1 = customArray.indexOf(4);

        Assertions.assertEquals(0, result);
        Assertions.assertEquals(-1, result1);
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#ensureCapacity(int)
     */
    @Test
    void ensureCapacity() {
        int result = customArray.getCapacity() + 4;

        customArray.ensureCapacity(4);

        Assertions.assertEquals(result, customArray.getCapacity());
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#getCapacity()
     */
    @Test
    void getCapacity() {

        Assertions.assertEquals(10, customArray.getCapacity());
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#reverse()
     */
    @Test
    void reverse() {

        customArray.add(1);
        customArray.add(2);
        customArray.add(3);

        CustomArrayImpl<Integer> array = new CustomArrayImpl<>(3);
        array.add(3);
        array.add(2);
        array.add(1);

        customArray.reverse();

        Assertions.assertEquals(array.toString(), customArray.toString());
    }
    /**
     * Проверка метода
     * @see CustomArrayImpl#toArray()
     */
    @Test
    void toArray() {

        customArray.add(1);
        customArray.add(2);
        customArray.add(3);

        Integer[] array = new Integer[3];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;

        Assertions.assertArrayEquals(array, customArray.toArray());

    }

    @Test
    void testToString() {
        customArray.add(1);
        customArray.add(2);
        customArray.add(3);

        String message = "[1, 2, 3]";

        Assertions.assertEquals(message, customArray.toString());
    }
}
