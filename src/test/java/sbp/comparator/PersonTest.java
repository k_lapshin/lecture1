package sbp.comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PersonTest {
    @Test
    void compareToTest() {

        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1,"Антон","Архангельск",12));
        personList.add(new Person(2,"Витя","Пермь",22));
        personList.add(new Person(3,"Ринат","Пермь",36));
        personList.add(new Person(4,"Степан","Волгоград",45));
        personList.add(new Person(5,"Борис","Пермь",64));

       Person person = new Person(3,"Ринат","Пермь",36);

        Assertions.assertEquals(person, personList.get(2));
        personList.sort(Person::compareTo);
        Assertions.assertEquals(person, personList.get(4));

        Assertions.assertThrows(RuntimeException.class, () -> new Person(0,null,null,0));
    }




}
