package sbp.comparator;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sbp.springConfig.SpringConfig;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class PersonJDBCTest {

    PersonDAO personDAO;
    @BeforeEach
    void beforeEach() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        personDAO = context.getBean(PersonDAO.class);
    }

    /**
     * Аннотация для удаления таблицы из базы, после выполнения каждого метода
     */

    @AfterEach
    void tearDown() {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        personDAO.dropTable();
    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#createTable()
     */

    @Test
    void testCreateTable() {
        boolean result = personDAO.createTable();
        Assertions.assertTrue(result);
    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#createTable()
     */

    @Test
    void testCreateTableError() throws SQLException {

        ServiceDAO serviceMock = Mockito.mock(ServiceDAO.class);
        Mockito.doThrow(new SQLException("Error")).when(serviceMock).executeUpdate(Mockito.anyString());

        PersonDAO personDAOTest = new PersonDAO(serviceMock);
        boolean result = personDAOTest.createTable();

        Assertions.assertFalse(result);

    }

    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#dropTable()
     */

    @Test
    void testDropTable() {
        personDAO.createTable();
        boolean result = personDAO.dropTable();
        Assertions.assertTrue(result);
    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#dropTable()
     */

    @Test
    void testDropTableError() throws SQLException {

        ServiceDAO serviceMock = Mockito.mock(ServiceDAO.class);
        Mockito.doThrow(new SQLException("Error")).when(serviceMock).executeUpdate(Mockito.anyString());

        PersonDAO personDAOTest = new PersonDAO(serviceMock);
        boolean result = personDAOTest.dropTable();

        Assertions.assertFalse(result);

    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#deletePerson(int)
     */

    @Test
    void testDeletePerson() {

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        personDAO.createTable();
        personDAO.addPerson(new Person(1, "Kol", "Volga", 10));
        personDAO.addPerson(new Person(2, "Vlad", "Volga", 12));
        personDAO.addPerson(new Person(3, "Pol", "Volga", 16));
        personDAO.addPerson(new Person(4, "Tom", "Volga", 18));
        personDAO.addPerson(new Person(5, "Ara", "Volga", 19));


        List<Integer> personList = new ArrayList<>();
        personList.add(1);
        personList.add(2);
        personList.add(3);
        personList.add(4);
        personList.add(5);

        List<Callable<Boolean>> calableList = new ArrayList<>();
        for (int i = 0; i < personList.size(); i++) {
            final int finalI = i;
            Callable task = new Callable() {
                @Override
                public Object call() throws Exception {
                    Assertions.assertTrue(personDAO.deletePerson(personList.get(finalI)));
                    return true;
                }
            };
            calableList.add(task);
        }

        try {
            List<Future<Boolean>> futures = executorService.invokeAll(calableList);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        boolean result = personDAO.deletePerson(12);
        Assertions.assertTrue(result);
    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#deletePerson(int)
     */

    @Test
    void testDeletePersonError() throws SQLException {

        personDAO.createTable();
        ServiceDAO serviceMock = Mockito.mock(ServiceDAO.class);
        Mockito.doThrow(new SQLException("Error")).when(serviceMock).executeUpdate(Mockito.anyString());

        PersonDAO personDAOTest = new PersonDAO(serviceMock);
        boolean result = personDAOTest.deletePerson(0);

        Assertions.assertFalse(result);

    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#addPerson(Person)
     */

    @Test
    void testAddPerson() {

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        System.out.println(personDAO.createTable());
        List<Person> personList = new ArrayList<>();
        personList.add(new Person(1, "Kol", "Volga", 10));
        personList.add(new Person(2, "Vlad", "Volga", 12));
        personList.add(new Person(3, "Pol", "Volga", 16));
        personList.add(new Person(4, "Tom", "Volga", 18));
        personList.add(new Person(5, "Ara", "Volga", 19));

        List<Callable<Boolean>> calableList = new ArrayList<>();
        for (int i = 0; i < personList.size(); i++) {
            final int finalI = i;
            Callable task = new Callable() {
                @Override
                public Object call() throws Exception {
                    Assertions.assertTrue(personDAO.addPerson(personList.get(finalI)));
                    return true;
                }
            };
            calableList.add(task);
        }

        try {
            List<Future<Boolean>> futures = executorService.invokeAll(calableList);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals(personDAO.readTableUniqueNumber(1), personList.get(0));
    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#addPerson(Person)
     */

    @Test
    void testAddPersonError() throws SQLException {

        personDAO.createTable();
        Person person = new Person(1, "name", "City", 20);

        ServiceDAO serviceMock = Mockito.mock(ServiceDAO.class);
        Mockito.doThrow(new SQLException("Error")).when(serviceMock).executeUpdate(Mockito.anyString());

        PersonDAO personDAOTest = new PersonDAO(serviceMock);
        boolean result = personDAOTest.addPerson(person);

        Assertions.assertFalse(result);

    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#updatePerson(int, String)
     */

    @Test
    void testUpdatePerson() {

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        personDAO.createTable();

        personDAO.addPerson(new Person(1, "Kol", "Perm", 10));
        personDAO.addPerson(new Person(2, "Vlad", "Volga", 12));
        personDAO.addPerson(new Person(3, "Pol", "Volga", 16));
        personDAO.addPerson(new Person(4, "Tom", "Volga", 18));
        personDAO.addPerson(new Person(5, "Ara", "Volga", 19));

        Person person = new Person(1, "Kol", "Perm", 10);
        List<String> personList = new ArrayList<>();
        personList.add("Maks");
        personList.add("Ivan");
        personList.add("Gleb");
        personList.add("Egor");

        List<Callable<Boolean>> calableList = new ArrayList<>();
        for (int i = 0; i < personList.size(); i++) {
            final int finalI = i;
            Callable task = new Callable() {
                @Override
                public Object call() throws Exception {
                    Assertions.assertTrue(personDAO.updatePerson(1, personList.get(finalI)));
                    System.out.println(Thread.currentThread().getName());
                    return true;
                }
            };
            calableList.add(task);
        }

        try {
            List<Future<Boolean>> futures = executorService.invokeAll(calableList);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (executorService.isShutdown()) {
            Person person1 = new Person(1, "Egor", "Perm", 10);
            Assertions.assertEquals(personDAO.readTableUniqueNumber(1), person1);
        }
    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeUpdate(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#updatePerson(int, String)
     */

    @Test
    void testUpdatePersonError() throws SQLException {

        personDAO.createTable();

        ServiceDAO serviceMock = Mockito.mock(ServiceDAO.class);
        Mockito.doThrow(new SQLException("Error")).when(serviceMock).executeUpdate(Mockito.anyString());

        PersonDAO personDAOTest = new PersonDAO(serviceMock);
        boolean result = personDAOTest.updatePerson(0, "Vlad");

        Assertions.assertFalse(result);

    }


    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeQuery(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#readTable()
     */

    @Test
    void testReadTable() {

        personDAO.createTable();

        Person person = new Person(142, "Ivan", "Moscow", 29);
        Person person1 = new Person(12, "Stepan", "Moscow", 45);
        personDAO.addPerson(person);
        personDAO.addPerson(person1);

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person1);

        Assertions.assertEquals(personDAO.readTable(), personList);
    }

    /**
     * Проверяет подключение к базе
     *
     * @see ServiceDAO#executeQuery(String)
     * Проверяет метод создания таблицы
     * @see PersonDAO#readTableUniqueNumber(int)
     */

    @Test
    void testReadTableUniqueNumber() {

        personDAO.createTable();

        Person person = new Person(142, "Ivan", "Moscow", 29);
        personDAO.addPerson(person);

        Assertions.assertEquals(personDAO.readTableUniqueNumber(142), person);

    }
}