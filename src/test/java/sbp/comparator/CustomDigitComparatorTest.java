package sbp.comparator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomDigitComparatorTest {
    @Test
    void compareTest() {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            list.add(i - 1);
        }

        Integer[] array = {0, 2, 4, 6, 8, 1, 3, 5, 7};

        List<Integer> list2 = new ArrayList<>();
        list2.add(null);
        list2.add(null);

        Collections.sort(list, new CustomDigitComparator());

        Assertions.assertArrayEquals(array, list.toArray());

        Assertions.assertThrows(RuntimeException.class, () ->
                Collections.sort(list2, new CustomDigitComparator()));
    }
}
