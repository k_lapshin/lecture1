package sbp.server;

import com.sun.net.httpserver.HttpExchange;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.Server.PersonHandler;
import sbp.comparator.Person;
import sbp.comparator.PersonDAO;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

class PersonHandlerTest {

    /**
     * Тест метода Get запрос
     * @see PersonHandler#handle(HttpExchange)
     * @throws IOException
     */
    @Test
    void handleGet() throws IOException {

        PersonDAO personDAO = Mockito.mock(PersonDAO.class);
        HttpExchange exchange = Mockito.mock(HttpExchange.class);
        OutputStream outputStream = Mockito.mock(OutputStream.class);
        List<Person> list = new ArrayList<>();
        PersonHandler personHandler = new PersonHandler(personDAO);

        Mockito.when(exchange.getRequestMethod()).thenReturn("GET");
        Mockito.when(exchange.getResponseBody()).thenReturn(outputStream);
        Mockito.when(personDAO.readTable()).thenReturn(list);

        personHandler.handle(exchange);
        Mockito.verify(personDAO, Mockito.times(1)).readTable();

    }

    /**
     * Тест метода Post запрос
     * @see PersonHandler#handle(HttpExchange)
     * @throws IOException
     */
    @Test
    void handlePost() throws IOException {
        JSONObject jsonObject = new JSONObject("{ " +
                "uniqueNumber" + ":" + 2 + "," +
                "name" + ":" + "a" + "," +
                "city" + ":" + "b" + "," +
                "age" + ":" + "2" +
                "}");

        InputStream inputStream2 = new ByteArrayInputStream(jsonObject.toString().getBytes(StandardCharsets.UTF_8));

        PersonDAO personDAO = Mockito.mock(PersonDAO.class);
        HttpExchange exchange = Mockito.mock(HttpExchange.class);
        OutputStream outputStream = Mockito.mock(OutputStream.class);
        InputStream inputStream = Mockito.mock(InputStream.class);
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);

        Person person = new Person(2, "a", "b", 2);

        PersonHandler personHandler = new PersonHandler(personDAO);
        Mockito.when(exchange.getRequestMethod()).thenReturn("POST");

        Mockito.when(exchange.getRequestBody()).thenReturn(inputStream2);

        Mockito.when(exchange.getResponseBody()).thenReturn(outputStream);

        Mockito.when(personDAO.addPerson(person)).thenReturn(true);

        personHandler.handle(exchange);
        Mockito.verify(personDAO, Mockito.times(1))
                .addPerson(person);

    }

    /**
     * Тест метода Delete запрос
     * @see PersonHandler#handle(HttpExchange)
     * @throws IOException
     */
    @Test
    void handleDelete() throws IOException {

        JSONObject jsonObject = new JSONObject("{ " +
                "uniqueNumber" + ":" + 2 + "}");

        InputStream inputStream = new ByteArrayInputStream(jsonObject.toString().getBytes(StandardCharsets.UTF_8));

        PersonDAO personDAO = Mockito.mock(PersonDAO.class);
        HttpExchange exchange = Mockito.mock(HttpExchange.class);
        OutputStream outputStream = Mockito.mock(OutputStream.class);

        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);

        PersonHandler personHandler = new PersonHandler(personDAO);
        Mockito.when(exchange.getRequestMethod()).thenReturn("DELETE");

        Mockito.when(exchange.getRequestBody()).thenReturn(inputStream);

        Mockito.when(exchange.getResponseBody()).thenReturn(outputStream);

        Mockito.when(personDAO.deletePerson(2)).thenReturn(true);

        personHandler.handle(exchange);
        Mockito.verify(personDAO, Mockito.times(1))
                .deletePerson(2);
    }

    /**
     * Тест метода Put запрос
     * @see PersonHandler#handle(HttpExchange)
     * @throws IOException
     */
    @Test
    void handleUpdate() throws IOException {

        JSONObject jsonObject = new JSONObject("{ " +
                "uniqueNumber" + ":" + "2" + "," +
                "name" + ":" + "a" + "}");

        InputStream inputStream = new ByteArrayInputStream(jsonObject.toString().getBytes(StandardCharsets.UTF_8));


        PersonDAO personJDBC = Mockito.mock(PersonDAO.class);
        HttpExchange exchange = Mockito.mock(HttpExchange.class);
        OutputStream outputStream = Mockito.mock(OutputStream.class);

        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);

        PersonHandler personHandler = new PersonHandler(personJDBC);
        Mockito.when(exchange.getRequestMethod()).thenReturn("PUT");

        Mockito.when(exchange.getRequestBody()).thenReturn(inputStream);

        Mockito.when(exchange.getResponseBody()).thenReturn(outputStream);

        Mockito.when(personJDBC.updatePerson(2,"a")).thenReturn(true);

        personHandler.handle(exchange);
        Mockito.verify(personJDBC, Mockito.times(1))
                .updatePerson(2,"a");
    }
}