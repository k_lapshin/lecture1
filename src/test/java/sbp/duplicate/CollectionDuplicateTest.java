package sbp.duplicate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CollectionDuplicateTest extends CollectionDuplicate {


    ArrayList<String> array = new ArrayList();

    /**
     * Проверка метода
     * @see CollectionDuplicate#collectionDuplicate(Collection)
     */
    @Test
    void testCollectionDuplicate() {
        for (int i = 0; i < 10; i++) {
            array.add(i + "_S");
        }
        array.add(1 + "_S");
        array.add(4 + "_S");
        array.add(6 + "_S");
        String[] result = {1 + "_S", 4 + "_S", 6 + "_S"};
        CollectionDuplicate call = new CollectionDuplicate();

        ArrayList<String> list = (ArrayList<String>) call.collectionDuplicate(array);

        Assertions.assertArrayEquals(result, list.toArray());
    }

    /**
     * Проверка метода
     * @see CollectionDuplicate#collectionDuplicate2(Collection)
     */
    @Test
    void testCollectionDuplicate2() {
        for (int i = 0; i < 10; i++) {
            array.add(i + "_S");
        }
        array.add(1 + "_S");
        array.add(4 + "_S");
        array.add(6 + "_S");

        String[] result = {1 + "_S", 4 + "_S", 6 + "_S"};
        CollectionDuplicate call = new CollectionDuplicate();
        ArrayList<String> list = (ArrayList<String>) call.collectionDuplicate(array);

        Assertions.assertArrayEquals(result, list.toArray());
    }


    /**
     * Проверка метода
     * @see CollectionDuplicate#collectionDuplicate3(Collection)
     */
    @Test
    void testCollectionDuplicate3() {
        for (int i = 0; i < 10; i++) {
            array.add(i + "_S");
        }
        array.add(1 + "_S");
        array.add(4 + "_S");
        array.add(6 + "_S");
        String[] result = {1 + "_S", 4 + "_S", 6 + "_S"};
        CollectionDuplicate call = new CollectionDuplicate();
        Collection list = call.collectionDuplicate3(array);

        Assertions.assertArrayEquals(result, list.toArray());
    }

    /**
     *  Проверка метода
     *  @see CollectionDuplicate#checkBrackets(String) 
     */
    @Test
    void testCheckBrackets() {
        String s = "(){}[]";
        String s1 = "[(])";
        String s2 = "({}[])";
        String s3 = ")";
        String s4 = "({[({((()))})]})";
        String s5 = "{([({)])}";

        CollectionDuplicate call = new CollectionDuplicate();

        boolean result = call.checkBrackets(s);
        boolean result1 = call.checkBrackets(s1);
        boolean result2 = call.checkBrackets(s2);
        boolean result3 = call.checkBrackets(s3);
        boolean result4 = call.checkBrackets(s4);
        boolean result5 = call.checkBrackets(s5);

        Assertions.assertEquals(true, result);
        Assertions.assertEquals(false, result1);
        Assertions.assertEquals(true, result2);
        Assertions.assertEquals(false, result3);
        Assertions.assertEquals(true, result4);
        Assertions.assertEquals(false, result5);
    }
}