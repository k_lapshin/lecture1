package sbp.restTemplate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import sbp.comparator.Person;
import sbp.comparator.PersonDAO;
import sbp.comparator.ServiceDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RestServerTest  {

    /**
     * Тест метода get запрос
     * @see RestServer#getHello()
     */
    @Test
    void testGetHello() {

        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        String list = new String();

        RestServer restServer = new RestServer(restTemplate);
        Mockito.when(restTemplate.getForEntity("http://localhost:8080/hello/get", String.class))
                .thenReturn(new ResponseEntity<>(list.toString(), HttpStatus.OK));
       String helloList =  restServer.getHello();

       Assertions.assertEquals(list,helloList.toString());


    }
    /**
     * Тест метода post запрос
     * @see RestServer#postHello(Person)
     */
    @Test
    void testPostHello() {

        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        Person person = new Person(4, "Television", "Samsung", 1145);
        RestServer restServer = new RestServer(restTemplate);

        Mockito.when(restTemplate.postForEntity("http://localhost:8080/hello/post",person, Person.class))
                .thenReturn(new ResponseEntity<>(person, HttpStatus.OK));

        String helloList =  restServer.postHello(person);

        Assertions.assertEquals(person.getName(),helloList);
    }
    /**
     * Тест метода put запрос
     * @see RestServer#putHello(Person)
     */
    @Test
    void testPutHello() {
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        Person person = new Person(4, "Telefon", "Samsung", 1145);
        HttpEntity<Person> entity = new HttpEntity(person);

        RestServer restServer = new RestServer(restTemplate);

        restServer.putHello(person);
        Mockito.verify(restTemplate, Mockito.times(1))
                .exchange("http://localhost:8080/hello/put",HttpMethod.PUT,entity,Void.class);
    }

    /**
     * Тест метода delete запрос
     * @see RestServer#deleteHello(Person)
     */
    @Test
    void testDeleteHello() {
        RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        Person person = new Person(4, "Telefon", "Samsung", 1145);
        HttpEntity<Person> entity = new HttpEntity(person);

        RestServer restServer = new RestServer(restTemplate);

        restServer.deleteHello(person);
        Mockito.verify(restTemplate, Mockito.times(1))
                .exchange("http://localhost:8080/hello/delete",HttpMethod.DELETE,entity,Void.class);
    }
}