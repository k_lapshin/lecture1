package sbp.restTemplate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import sbp.comparator.Person;
import sbp.comparator.PersonDAO;

import java.util.ArrayList;
import java.util.List;

class ControllerTest  {

    Controller controller;

    /**
     * Тест метода get запрос
     * @see Controller#sampleGetMethod()
     */
    @Test
    void testSampleGetMethod() {
        Person person = new Person(4, "Telefon", "Samsung", 1145);
        PersonDAO personDAO = Mockito.mock(PersonDAO.class);
        List<Person> list = new ArrayList<>();

        Mockito.when(personDAO.readTable()).thenReturn(list);

        controller = new Controller(personDAO);
        ResponseEntity<String> responseEntity = controller.sampleGetMethod();

        Assertions.assertEquals(responseEntity.getStatusCodeValue(),200);
        Mockito.verify(personDAO, Mockito.times(1))
                .readTable();
    }

    /**
     * Тест метода post запрос
     * @see Controller#samplePostMethod(Person)
     */
    @Test
    void testSamplePostMethod() {
        Person person = new Person(4, "Telefon", "Samsung", 1145);
        PersonDAO personDAO = Mockito.mock(PersonDAO.class);

        Mockito.when(personDAO.addPerson(person)).thenReturn(true);
        controller = new Controller(personDAO);
        ResponseEntity<String> responseEntity = controller.samplePostMethod(person);

        Assertions.assertEquals(responseEntity.getStatusCodeValue(),200);
        Assertions.assertEquals(responseEntity.getBody(), person.getName());
        Mockito.verify(personDAO, Mockito.times(1))
                .addPerson(person);
    }

    /**
     * Тест метода put запрос
     * @see Controller#samplePutMethod(Person)
     */
    @Test
    void testSamplePutMethod() {
        Person person = new Person(4, "Telefon", "Samsung", 1145);
        PersonDAO personDAO = Mockito.mock(PersonDAO.class);

        Mockito.when(personDAO.addPerson(person)).thenReturn(true);
        controller = new Controller(personDAO);
        ResponseEntity<String> responseEntity = controller.samplePutMethod(person);

        Assertions.assertEquals(responseEntity.getStatusCodeValue(),200);
        Assertions.assertEquals(responseEntity.getBody(), person.getName());
        Mockito.verify(personDAO, Mockito.times(1))
                .updatePerson(4,"Telefon");
    }

    /**
     * Тест метода delete запрос
     * @see Controller#sampleDeleteMethod(Person)
     */
    @Test
    void testSampleDeleteMethod() {
        Person person = new Person(4, "Telefon", "Samsung", 1145);
        PersonDAO personDAO = Mockito.mock(PersonDAO.class);

        Mockito.when(personDAO.deletePerson(4)).thenReturn(true);
        controller = new Controller(personDAO);
        ResponseEntity<String> responseEntity = controller.sampleDeleteMethod(person);

        Assertions.assertEquals(responseEntity.getStatusCodeValue(),200);
        Assertions.assertEquals(responseEntity.getBody(), person.getName());
        Mockito.verify(personDAO, Mockito.times(1))
                .deletePerson(4);
    }
}